CREATE DATABASE IF NOT EXISTS db_utility   
CHARACTER SET utf8
COLLATE utf8_general_ci;

USE db_utility;

DROP TABLE IF EXISTS log_msg;
CREATE TABLE log_msg
  ( val varchar( 3000 ) ); 

delimiter //
DROP PROCEDURE IF EXISTS fix_protocol_values//

CREATE PROCEDURE fix_protocol_values(IN in_option varchar(10))
proc_label:BEGIN
    DECLARE done INT DEFAULT 0;
    DECLARE db VARCHAR(255);
    DECLARE ctr int;
    DECLARE db2 VARCHAR(255);
    DECLARE appDBs CURSOR FOR SELECT schema_name FROM information_schema.schemata WHERE schema_name NOT IN ('reflexion_vera_auth') AND schema_name LIKE 'reflexion_vera_%';
    DECLARE appDBs2 CURSOR FOR SELECT schema_name FROM information_schema.schemata WHERE schema_name NOT IN ('reflexion_vera_auth') AND schema_name LIKE 'reflexion_vera_%';
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;
    
    IF in_option NOT IN ( 'fix', 'report' ) THEN
          SELECT 'Must enter valid parameter: ''fix'' or ''report''';
          LEAVE proc_label;
    END IF;
  
    DROP TABLE IF EXISTS output;
    CREATE TEMPORARY TABLE output 
    (
      database_name varchar(100) NOT NULL  
    , protocol_name varchar(100) NOT NULL
    , protocol_id   binary(16) NOT NULL
    , protocol_exercises_id   binary(16) NOT NULL
    , template_id              binary(16) NOT NULL
    , protocol_free_motion integer NOT NULL 
    , exercise_name varchar(100) NOT NULL
    , free_motion_fixed integer NOT NULL 
    , free_motion_default integer NOT NULL 
    ) ENGINE=MEMORY;

    DELETE FROM log_msg;

    IF in_option IN ( 'fix', 'report' ) THEN

      OPEN appDBs;
          REPEAT
              FETCH appDBs INTO db;
              IF NOT done THEN
      
  
              Set @v_sql1 = concat('  INSERT INTO output
                                      SELECT 
                                        "', db ,'" AS db_name,
                                      p.name AS protocol_name,
                                      p.id AS protocol_id,
                                      pe.id AS protocol_exercises_id,
                                      et.id AS template_id,
                                      pe.free_motion AS protocol_free_motion, 
                                      et.name AS exercise_name, 
                                      et.free_motion_fixed, 
                                      et.free_motion_default 
                                      FROM `', db ,'`.protocols p 
                                     JOIN `', db ,'`.protocol_exercises pe ON pe.protocol_id = p.id    
                                     JOIN `', db ,'`.exercise_templates et ON et.id = pe.template_id
                                    WHERE pe.free_motion = 0 AND et.free_motion_fixed = 1;
                                  ' );
                PREPARE stmt1 from @v_sql1;
          			EXECUTE stmt1;
                          
                SELECT ROW_COUNT() INTO ctr;
  
                INSERT INTO log_msg
                SELECT CONCAT( ctr, ' records found in`', db ,'` database' );
  
              END IF;
          UNTIL done 
        END REPEAT;
      CLOSE appDBs;
    END IF;


    IF in_option = 'fix' THEN

      set done=0;

      OPEN appDBs2;
        REPEAT
            FETCH appDBs2 INTO db2;
            IF NOT done THEN
 
            Set @v_sql2 = concat( 'UPDATE `', db2 ,'`.protocol_exercises SET free_motion = 1 WHERE id IN ( SELECT protocol_exercises_id FROM output );' );
              PREPARE stmt2 from @v_sql2;
        			EXECUTE stmt2;
 
              SELECT ROW_COUNT() INTO ctr;

              INSERT INTO log_msg
              SELECT CONCAT( ctr, ' records updated in`', db2 ,'` database' ); 
                         
            END IF;
        UNTIL done 
      END REPEAT;
     CLOSE appDBs2;
    
    END IF;
      
     SELECT COUNT(*) INTO ctr FROM log_msg;

      IF ctr > 0 THEN
      
        SELECT * FROM log_msg;
  
      ELSE
  
        SELECT 'No issues found';      
  
      END IF;
        
END//
DELIMITER ;