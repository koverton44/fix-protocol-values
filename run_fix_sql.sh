#!/bin/bash
set -e

red=`tput setaf 1`
green=`tput setaf 2`
magenta=`tput setaf 5`
reset=`tput sgr0`

source '.ENV'
echo $SQL_PARAM
if [ "$SQL_PARAM" != 'fix' ] && [ "$SQL_PARAM" != 'report' ]; then
   echo "${red}Please input "report" or "fix" for SQL_PARAM${reset}"
   exit 1
fi

echo "${green}Running SQL update scripts...${reset}"
mysql -h$DB_DST_HOST -P$DB_DST_PORT -u$DB_DST_USER -p$DB_DST_PWD < ./fix_protocol_values.sql;
mysql -h$DB_DST_HOST -P$DB_DST_PORT -u$DB_DST_USER -p$DB_DST_PWD -e "USE db_utility; CALL fix_protocol_values('"$SQL_PARAM"');"

exit 1