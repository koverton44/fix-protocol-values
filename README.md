# fix-protocol-values

###Prerequisites
	
- A MySQL account with ccess to the MySQL Server with the clients you are trying to update

###Usage

- This utility offers two options for fixing Fix Tracked StepUp protocol issue

	1. Make sure you have recent backups of your databases prior to execution.
	2. Option 1('report'): This will output a list of clients who have the free_motion bit == 0 
		while the free_motion_fixed bit == 1 in the corresponding exercise_template item.
	3. Option 2('fix'): This will first output the same list as option 2, but the utility will
		update the records identified and resolve the issue by setting protocol_exercises.free_motion := 1  
		if  exercise_template.free_motion_fixed == 1.  A list of updated number of client records will display 
		that should match what was reported.  
	4. When utility is run the first time it will create a database called db_utility that will function as utility
		database for future task like this. It should not be deleted.
		
###Instructions   


	1.	Clone this repo: git clone git@bitbucket.org:reflexionhealthinc/fix-protocol-values.git
	2.	Make the following edits to the .ENV file

```bash
		DB_DST_HOST=127.0.0.1  	-- MySQL SERVER YOU ARE USING.  
		DB_DST_PORT=3307  		-- PORT OF MySQL SERVER
		DB_DST_USER=reflexion 	-- ACCOUNT USER WITH ACCESS
		DB_DST_PWD=r******** 	-- ACCOUNT PASSWORD
		DSQL_PARAM=fix			-- UTILITY RUN OPTION: 'report'=Get list of affected clients, 'fix'=Fix affected clients
```

	3.	Go into ../fix-protocol-values directory and run ./run_fix_sql.sh

		 
		 
		 



